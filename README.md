# Students List 2023

| # | Student | Current progress |
| ------ | ------ | ----- |
|   1     |   [Бацкель Илья Артурович](https://gitlab.com/ilya1semen/ilyabatskell.homework)     | 4 |
|   2     |   [Верещако София Владимировна](https://gitlab.com/Valeria-l/all_homework)    | 5 |
|   3     |   [Голяк Вероника Юрьевна](https://gitlab.com/22541938/all_homework)   | 5 |
|   4     |   [Дылевский Максим Сергеевич](https://gitlab.com/dylevski)     | 9 |
|   5     |   [Коновалов Иван Сергеевич](https://gitlab.com/bsu-2023-2024)     | 10 |
|   6     |   [Лашук Алексей Дмитриевич](https://gitlab.com/saint-blase/homework)    | 4 |
|   7     |   [Мирончик Илья Дмитриевич](https://gitlab.com/mmf-mironchikilya/1-semester)   | 4 |
|   8     |   [Овсяник Валерия Сергеевна](https://gitlab.com/Valeria-l/all_homework)   | 5 |
|   9     |   [Перцовой Ян Дмитриевич](https://gitlab.com/YanPertsavy)    | 7 |
|   10     |   [Сечко Кирилл Александрович](https://gitlab.com/sechkokirill679/homework)    | 4 |
|   11     |   [Сокольников Иван Витальевич](https://gitlab.com/Rediska23)   | 4 |
|   12     |   [Таразевич Владислав Павлович](https://gitlab.com/NemoCrab/day-6-4-10-2023)    | 7 |
|   13     |   [Шарко Алексей Игоревич](https://gitlab.com/AlekseyMikn)   | 8 |
|   14     |   [Яцкевич Дмитрий Павлович](https://gitlab.com/1-semester/1-semester-homework)   | 10 |
